const DataTypes = require('sequelize/lib/data-types');

module.exports = (sequelize, Sequelize) => {
    const Academicyear = sequelize.define("academic_years", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: Sequelize.STRING
        },
        slug: {
            type: Sequelize.STRING
        },
        is_current: {
            type: Sequelize.BOOLEAN
        },
        is_next: {
            type: Sequelize.BOOLEAN
        },
        from_date: { 
            type: DataTypes.DATEONLY
        },
        to_date: { 
            type: DataTypes.DATEONLY
        },
        createdAt: { 
            type: Sequelize.DATE,
            allowNull: true
        },
        updatedAt: { 
            type: Sequelize.DATE,
            allowNull: false,
            defaultValue: Sequelize.NOW
        },
        deletedAt: { 
            type: Sequelize.DATE,
            allowNull: true
        }
    });
  
    return Academicyear;
  };