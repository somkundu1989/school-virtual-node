module.exports = app => {
    const academicyear = require("../controllers/academicyear.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", academicyear.create);
  
    // Retrieve all Tutorials
    router.get("/", academicyear.findAll);
  
    // Retrieve all published Tutorials
    // router.get("/published", academicyear.findAllPublished);
  
    // Retrieve a single Tutorial with id
    router.get("/:id", academicyear.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", academicyear.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", academicyear.delete);
  
    // Create a new Tutorial
    router.delete("/", academicyear.deleteAll);
  
    app.use('/api/academicyears', router);
  };