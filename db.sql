-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 02, 2020 at 10:20 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `virtualschool`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_years`
--

CREATE TABLE `academic_years` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_date` varchar(20) DEFAULT NULL,
  `to_date` varchar(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `is_activate` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: active, 0: inactive	',
  `is_next` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: active, 0: inactive	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_years`
--

INSERT INTO `academic_years` (`id`, `from_date`, `to_date`, `title`, `slug`, `is_activate`, `is_next`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '01/03/2020', '29/02/2021', '2020 2021', '2020-2021', 1, 0, '2019-10-09 06:55:03', '2020-01-03 02:43:23', NULL),
(2, '01/03/2021', '28/02/2022', '2021-2022', '2021-2022', 0, 1, '2019-10-09 06:55:45', '2019-12-18 01:42:11', NULL),
(6, '01/03/2021', '28/02/2022', '2021-2022', '2021-2022-1', 0, 1, '2019-10-09 06:55:45', '2019-12-18 01:42:11', '2020-03-08 06:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `teacher_id` bigint(20) UNSIGNED DEFAULT NULL,
  `classsession_id` int(10) UNSIGNED DEFAULT NULL,
  `last_submission_date` varchar(20) DEFAULT NULL,
  `instructions` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `attachments` text,
  `total_marks` float(5,2) NOT NULL DEFAULT '100.00',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: drafted, 1: complete',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_attachment`
--

CREATE TABLE `assignment_attachment` (
  `id` int(10) UNSIGNED NOT NULL,
  `assignment_id` int(10) UNSIGNED DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_student`
--

CREATE TABLE `assignment_student` (
  `id` int(10) UNSIGNED NOT NULL,
  `assignment_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `answer` text,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:pending, 1: submitted, 2:drafted, 3: reviewed-by-teacher-or-tutor',
  `marks` float(5,2) DEFAULT NULL,
  `submitted_at` varchar(20) DEFAULT NULL,
  `evaluated_at` varchar(20) DEFAULT NULL,
  `remarks` text,
  `marks_per_questions` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_student_attachment`
--

CREATE TABLE `assignment_student_attachment` (
  `id` int(10) UNSIGNED NOT NULL,
  `assignment_student_id` int(10) UNSIGNED DEFAULT NULL,
  `attachment` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` varchar(50) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `classsessions`
--

CREATE TABLE `classsessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `start_time` varchar(20) DEFAULT NULL,
  `end_time` varchar(20) DEFAULT NULL,
  `teacher_id` bigint(20) UNSIGNED DEFAULT NULL,
  `occurred_date` varchar(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `chat_or_call_external_link` varchar(255) DEFAULT NULL,
  `external_verdor_name` varchar(255) DEFAULT NULL,
  `chat_or_call_saved_link` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: setup-need-to-complete,1: setup-complete, 2: class-passed/completed, 3: class-cancelled, 4: started',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `classsession_students`
--

CREATE TABLE `classsession_students` (
  `id` int(10) UNSIGNED NOT NULL,
  `classsession_id` int(10) UNSIGNED DEFAULT NULL,
  `teacher_id` bigint(20) UNSIGNED DEFAULT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_attended` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: attended, 0: not-attended',
  `rating` tinyint(1) DEFAULT '0' COMMENT '1 to 5',
  `comment` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `report_date` date DEFAULT NULL,
  `report_description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@vclass.com', '$2y$10$.6ms7/EbEcO/InhQRD3uO.g31jokxRKyZa1/dZrVqvSUABsOKv3F2', '2019-08-26 07:50:55'),
('sourav.dey@dreamztech.com', '$2y$10$mLA9OXUtb98lHn1c29hdxumxE1cJFjH6ZyxfJj7PZ7w76elJdzsR.', '2019-11-18 02:44:53'),
('tester@mail.com', '$2y$10$wVASumoZjIuqBIFdCcw4/.RNSePtwjQAdvG/hPiHU3s8vXztTK1rm', '2019-11-26 04:05:03'),
('bruce08@yopmail.com', '$2y$10$JiSwfIHPx9zKw/Kzk6K6U.cJQzc6VxtLh7Kon8Wdv.08WMFjD2lTu', '2019-12-10 07:26:20'),
('subhamnayak@yopmail.com', '$2y$10$iPFeTt63PFj8frEycra.h.yMbGIcS18LMEPRpJj9W.w0W9Fko7qj6', '2019-12-10 07:26:56'),
('sourav.dey@gmail.com', '$2y$10$iTavT0rSF4w06pppvx5rcuaMXX.jLl1LosbTfcB.dMkRBtpIBM5vK', '2019-12-10 08:47:14'),
('divakar.bagari@dreamztech.com', '$2y$10$m1zsqsMY4SBs.J1tz6ntyukB8SeC0NRKkkLSmDAi7tjkHPYUaDOua', '2019-12-24 05:25:39'),
('moumita.hazra@dreamztech.com', '$2y$10$yYoqTHiyiC6s.dbRz3eSbe3DCH9HEQcqO/CUYm26/bV5aTPkSgqYG', '2019-12-24 06:06:38'),
('admin@admin.com', '$2y$10$A5/qTqBPYFnt5aSdI.7tGeouBcT99voZSRlH4gAYOPbA8dsDd1WKS', '2019-12-24 06:34:41'),
('dhiman77@yopmail.com', '$2y$10$wGuqZNdR4uurx5lozPlF0uS8tfVlMXUBrRWLStFJgWlKXQCz1OkRm', '2019-12-30 10:04:42');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'profile.edit', 'Profile Edit', NULL, '2007-12-07 15:17:42', '2002-04-19 08:59:29', NULL),
(2, 'home', 'Dashboard', NULL, '1988-04-23 01:52:21', '2019-11-18 23:29:04', NULL),
(3, 'systemadministrators.index', 'System Administrators List', NULL, '1989-10-26 15:26:08', '1992-05-09 04:55:07', NULL),
(4, 'systemadministrators.edit', 'System Administrators Edit', NULL, '2016-03-14 02:19:10', '1976-02-27 04:12:05', NULL),
(5, 'supportadministrators.index', 'Support Administrators List', NULL, '2007-01-18 06:51:51', '1982-12-30 05:32:23', NULL),
(6, 'supportadministrators.create', 'Support Administrators Add', NULL, '1972-04-14 10:40:13', '1988-06-02 02:33:44', NULL),
(7, 'supportadministrators.edit', 'Support Administrators Edit', NULL, '2012-08-03 08:53:34', '1984-09-16 19:59:11', NULL),
(8, 'supportadministrators.change-status', 'Support Administrators Change Status', NULL, '2007-03-07 09:59:01', '1988-10-22 02:55:08', NULL),
(9, 'principals.index', 'Principals List', NULL, '2001-02-02 23:53:17', '2019-03-16 02:50:24', NULL),
(10, 'principals.create', 'Principals Add', NULL, '2006-07-04 15:45:07', '2005-09-07 16:57:56', NULL),
(11, 'principals.edit', 'Principals Edit', NULL, '2016-05-07 11:02:32', '1975-06-19 03:00:28', NULL),
(12, 'principals.change-status', 'Principals Change Status', NULL, '1973-05-16 15:49:13', '1979-03-03 00:27:35', NULL),
(13, 'inspectors.index', 'Inspectors List', NULL, '2014-08-11 21:58:53', '1977-08-18 05:28:30', NULL),
(14, 'inspectors.create', 'Inspectors Add', NULL, '1983-05-23 20:20:16', '1970-07-16 19:44:38', NULL),
(15, 'inspectors.edit', 'Inspectors Edit', NULL, '2008-02-13 21:08:15', '2013-05-27 07:20:46', NULL),
(16, 'inspectors.change-status', 'Inspectors Change Status', NULL, '1979-02-10 12:33:04', '2013-12-01 09:13:19', NULL),
(17, 'teachers.index', 'Teachers List', NULL, '1970-09-21 16:44:00', '1987-04-25 22:48:09', NULL),
(18, 'teachers.create', 'Teachers Add', NULL, '2012-09-15 17:38:35', '2014-03-14 12:24:07', NULL),
(19, 'teachers.edit', 'Teachers Edit', NULL, '2002-08-14 06:15:42', '1980-01-15 17:26:31', NULL),
(20, 'teachers.change-status', 'Teachers Change Status', NULL, '1997-07-29 03:18:16', '1982-05-27 00:30:03', NULL),
(21, 'tutors.index', 'Tutors List', NULL, '1977-03-02 21:27:48', '1977-03-20 14:49:40', NULL),
(22, 'tutors.create', 'Tutors Add', NULL, '1973-11-23 05:00:58', '2009-01-11 19:38:25', NULL),
(23, 'tutors.edit', 'Tutors Edit', NULL, '1982-12-23 05:47:08', '2008-12-02 01:11:02', NULL),
(24, 'tutors.change-status', 'Tutors Change Status', NULL, '1984-06-06 18:14:59', '1975-03-18 05:13:35', NULL),
(25, 'parents.index', 'Parents List', NULL, '1978-03-17 01:48:09', '2005-12-17 06:27:44', NULL),
(26, 'parents.edit', 'Parents Edit', NULL, '1994-09-08 11:51:10', '1983-08-28 14:01:28', NULL),
(27, 'parents.change-status', 'Parents Change Status', NULL, '2017-05-18 09:21:54', '2014-09-30 15:51:13', NULL),
(28, 'students.index', 'Students List', NULL, '2019-01-05 14:44:07', '1972-03-28 15:36:44', NULL),
(29, 'students.edit', 'Students Edit', NULL, '2003-10-06 15:55:17', '2004-06-25 17:02:13', NULL),
(30, 'students.change-status', 'Students Change Status', NULL, '2006-01-06 17:29:56', '1990-01-31 22:19:44', NULL),
(31, 'students.parent-wise', 'Students Parent Wise View', NULL, '1989-01-22 17:29:17', '1981-03-03 00:46:33', NULL),
(32, 'payments.index', 'Payments List', NULL, '1979-04-11 00:40:45', '2003-11-11 19:54:56', NULL),
(33, 'schoolconfigurations.edit', 'Institution Edit', NULL, '2012-04-08 09:58:08', '2010-12-05 04:46:38', NULL),
(34, 'schoolconfigurations.change-status', 'Institution Change Status', NULL, '2001-09-02 15:49:36', '1991-06-16 17:51:59', NULL),
(35, 'academicyears.index', 'Academic Years List', NULL, '1979-08-10 08:13:28', '1987-02-16 14:07:33', NULL),
(36, 'academicyears.create', 'Academic Years Add', NULL, '2006-06-04 16:36:38', '2018-11-27 00:50:26', NULL),
(37, 'academicyears.edit', 'Academic Years Edit', NULL, '1994-03-02 17:25:58', '1993-05-23 05:54:34', NULL),
(38, 'academicyears.change-status', 'Academic Years Change Status', NULL, '1990-01-19 14:46:41', '2013-10-30 21:56:40', NULL),
(39, 'grades.index', 'Grades List', NULL, '1985-11-24 03:11:15', '2016-09-18 14:01:44', NULL),
(40, 'grades.create', 'Grades Add', NULL, '1981-05-23 19:15:11', '2019-03-05 09:40:21', NULL),
(41, 'grades.edit', 'Grades Edit', NULL, '2014-03-18 03:26:29', '2013-06-03 16:52:13', NULL),
(42, 'subscriptionfees.index', 'Subscription Fees List', NULL, '1978-04-14 13:22:41', '2001-01-24 23:44:31', NULL),
(43, 'subscriptionfees.create', 'Subscription Fees Add', NULL, '1970-12-08 10:13:10', '2010-02-12 21:20:26', NULL),
(44, 'subscriptionfees.edit', 'Subscription Fees Edit', NULL, '1995-09-02 22:11:49', '1992-04-15 02:18:02', NULL),
(45, 'subscriptionfees.destroy', 'Subscription Fees Delete', NULL, '1991-02-19 00:25:55', '1997-03-31 20:45:40', NULL),
(46, 'subscriptionfees.change-status', 'Subscription Fees Change Status', NULL, '1981-11-04 16:21:22', '1992-01-12 14:49:41', NULL),
(47, 'subjects.index', 'Subjects List', NULL, '2019-04-26 14:57:08', '1975-01-28 19:55:04', NULL),
(48, 'subjects.create', 'Subjects Add', NULL, '2008-01-28 09:25:38', '2010-10-09 08:06:00', NULL),
(49, 'subjects.edit', 'Subjects Edit', NULL, '1993-06-19 15:22:10', '1992-09-08 23:31:28', NULL),
(50, 'subjects.change-status', 'Subjects Change Status', NULL, '1999-02-26 18:37:25', '1976-09-10 16:43:03', NULL),
(51, 'virtualclasses.index', 'Class Schedules List', NULL, '2019-08-11 17:07:58', '1985-01-31 01:36:22', NULL),
(52, 'virtualclasses.create', 'Class Schedules Add', NULL, '2006-06-21 13:15:58', '1994-06-11 06:53:57', NULL),
(53, 'virtualclasses.edit', 'Class Schedules Edit', NULL, '1977-05-08 22:20:09', '2013-03-15 08:54:30', NULL),
(54, 'virtualclasses.destroy', 'Class Schedules Delete', NULL, '1992-08-06 20:55:16', '1997-08-08 10:32:39', NULL),
(55, 'tutorsvirtualclasses.index', 'Tutor Sessions List', NULL, '2012-11-20 17:42:59', '2001-10-01 17:58:21', NULL),
(56, 'tutorsvirtualclasses.edit', 'Tutor Sessions Edit', NULL, '2003-01-02 22:28:06', '1985-08-04 08:02:49', NULL),
(57, 'assignments.index', 'Assignments List', NULL, '2000-11-22 06:10:01', '1973-11-03 23:38:53', NULL),
(58, 'feedbacks.index', 'Feedbacks List', NULL, '1990-08-11 03:44:40', '2012-01-05 11:30:35', NULL),
(59, 'roles.index', 'User types List', NULL, '1985-12-25 20:23:04', '1989-06-13 19:07:06', NULL),
(60, 'assignmentstudents.index', 'Evaluate Assignments List', NULL, '2003-01-22 13:16:03', '2002-04-26 12:39:04', NULL),
(61, 'assignmentstudents.edit', 'Evaluate Assignments Edit', NULL, '1973-05-03 13:04:37', '1982-06-14 15:39:20', NULL),
(62, 'virtualclassstudents.index', 'Virtual Class Student Feedbacks List', NULL, '1986-04-13 22:13:06', '1997-06-29 16:16:43', NULL),
(63, 'assignments.create', 'Assignments Add', NULL, '2009-12-19 19:34:51', '1996-05-11 11:04:52', NULL),
(64, 'assignments.edit', 'Assignments Edit', NULL, '1994-12-05 17:15:29', '2015-10-12 04:22:27', NULL),
(65, 'assignments.show', 'Assignments Details', NULL, '1970-07-27 13:06:36', '2016-08-12 18:14:09', NULL),
(66, 'virtualclasses.cancel-approve', 'Class Schedules Approve or Reject Class Schedules', NULL, '1974-07-01 03:27:48', '1996-10-29 16:34:09', NULL),
(67, 'tutorsvirtualclasses.create', 'Tutor Sessions Add', NULL, '1994-09-12 09:29:31', '2008-08-02 05:36:09', NULL),
(68, 'tutorsvirtualclasses.cancel-approve', 'Tutor Sessions Approve or Reject Class Schedules', NULL, '1993-04-03 21:39:49', '2009-07-10 15:11:49', NULL),
(69, 'requestvirtualclasses.index', 'Request Tutor Sessions List', NULL, '1987-12-20 16:48:05', '1989-11-18 06:52:56', NULL),
(70, 'requestvirtualclasses.show', 'Request Tutor Sessions Details', NULL, '1983-01-08 23:32:40', '1977-08-21 00:06:28', NULL),
(71, 'parents.create', 'Parents Add', NULL, '1981-04-02 07:57:01', '1983-09-13 03:20:29', NULL),
(72, 'virtualclasses.box-index', 'Class Schedules box-List', NULL, '1988-04-27 21:28:55', '1984-06-08 07:40:27', NULL),
(73, 'virtualclasses.join', 'Class Schedules Can Join', NULL, '1990-01-17 22:19:15', '2015-03-23 06:43:08', NULL),
(74, 'tutorsvirtualclasses.box-index', 'Tutor Sessions box-List', NULL, '2005-07-14 19:32:29', '2010-05-13 04:20:22', NULL),
(75, 'tutorsvirtualclasses.join', 'Tutor Sessions Can Join', NULL, '2010-03-18 03:11:26', '2008-04-28 17:09:37', NULL),
(76, 'virtualclassstudents.create', 'Virtual Class Student Feedbacks/Attendence Add', NULL, '1979-08-15 16:16:26', '2004-07-23 01:04:19', NULL),
(77, 'virtualclassstudents.show', 'Virtual Class Student Feedbacks/Attendence Details', NULL, '1990-12-08 00:19:42', '2006-08-09 21:35:49', NULL),
(78, 'virtualclassstudents.attendence', 'Virtual Class Student Feedbacks/Attendence Attendence', NULL, '2015-07-30 18:19:56', '2014-04-17 22:06:25', NULL),
(79, 'requestvirtualclasses.create', 'Request Tutor Sessions Add', NULL, '1985-05-06 17:17:24', '1979-01-22 20:24:56', NULL),
(80, 'students.change-grade', 'Students Upgrade', NULL, '2007-11-26 05:02:01', '1970-12-11 05:54:12', NULL),
(81, 'payments.create', 'Payments Add', NULL, '1994-08-01 05:15:23', '1997-03-27 06:49:54', NULL),
(82, 'students.create', 'Students Add', NULL, '1985-10-25 21:54:47', '2008-07-18 04:39:17', NULL),
(83, 'students.unlink', 'Students Can Unlink Student', NULL, '1983-02-20 14:44:48', '1993-03-27 20:14:23', NULL),
(84, 'supportadministrators.view-only', 'Support Administrators View Form Only', NULL, '2003-07-13 19:43:03', '2013-08-11 19:50:34', NULL),
(85, 'principals.view-only', 'Principals View Form Only', NULL, '1984-11-06 18:52:00', '2018-07-10 01:03:38', NULL),
(86, 'inspectors.view-only', 'Inspectors View Form Only', NULL, '1999-03-12 15:50:37', '2018-09-24 18:06:32', NULL),
(87, 'teachers.view-only', 'Teachers View Form Only', NULL, '1991-06-24 05:42:15', '2012-04-20 10:00:37', NULL),
(88, 'tutors.view-only', 'Tutors View Form Only', NULL, '1981-03-10 12:09:55', '1998-03-26 00:37:31', NULL),
(89, 'parents.view-only', 'Parents View Form Only', NULL, '2012-05-01 11:46:23', '1981-11-02 03:31:16', NULL),
(90, 'students.view-only', 'Students View Form Only', NULL, '2003-08-07 09:37:49', '1987-08-19 02:18:22', NULL),
(91, 'virtualclasses.parent-wise', 'Class Schedules Parent Wise View', NULL, '1979-06-01 14:39:46', '2000-07-16 05:29:06', NULL),
(92, 'reportabuses.index', 'Reports List', NULL, '1974-08-02 05:21:38', '1999-02-20 09:27:06', NULL),
(93, 'virtualclasses.view-only', 'Class Schedules View Form Only', NULL, '1989-02-03 01:32:09', '1983-09-11 07:35:26', NULL),
(94, 'tutorsvirtualclasses.view-only', 'Tutor Sessions View Form Only', NULL, '2000-06-30 00:46:52', '1997-12-29 03:31:39', NULL),
(95, 'reportabuses.create', 'Reports Add', NULL, '1980-06-24 13:21:10', '1972-10-30 13:28:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2003-12-31 06:38:15', '2008-06-12 19:51:38', NULL),
(2, 2, 1, '1989-01-04 21:35:37', '1976-07-11 02:41:57', NULL),
(3, 3, 1, '1977-06-25 00:30:21', '1984-12-04 12:40:08', NULL),
(4, 4, 1, '1998-11-02 11:05:04', '1997-06-06 15:27:11', NULL),
(5, 5, 1, '1994-04-17 08:08:53', '2008-02-21 21:20:41', NULL),
(6, 6, 1, '2000-01-02 18:31:37', '1975-08-20 21:47:45', NULL),
(7, 7, 1, '1972-11-17 05:12:53', '2018-03-05 14:17:17', NULL),
(8, 8, 1, '2005-03-22 05:19:22', '1980-09-27 04:42:46', NULL),
(9, 9, 1, '1998-11-06 07:55:59', '1982-09-21 03:00:31', NULL),
(10, 10, 1, '1970-01-07 01:21:32', '1989-10-18 22:03:29', NULL),
(11, 11, 1, '2007-06-29 02:07:21', '2014-06-07 05:17:31', NULL),
(12, 12, 1, '1996-02-22 03:23:42', '1985-11-30 01:38:29', NULL),
(13, 13, 1, '1978-09-14 21:29:03', '2014-06-07 08:44:24', NULL),
(14, 14, 1, '2009-02-23 23:59:08', '1991-12-21 07:01:29', NULL),
(15, 15, 1, '1990-04-16 09:54:36', '2016-06-10 02:27:44', NULL),
(16, 16, 1, '2001-05-05 01:46:15', '2013-05-03 03:03:23', NULL),
(17, 17, 1, '2003-10-30 17:16:33', '1985-05-04 20:24:23', NULL),
(18, 18, 1, '1984-03-09 14:40:01', '1981-12-24 22:03:24', NULL),
(19, 19, 1, '1992-02-18 12:55:28', '1985-04-06 12:32:50', NULL),
(20, 20, 1, '1978-11-27 01:55:39', '2008-06-05 06:06:59', NULL),
(21, 21, 1, '1994-03-15 02:32:58', '1995-05-06 09:59:07', NULL),
(22, 22, 1, '2004-09-13 04:03:55', '2006-10-06 21:45:24', NULL),
(23, 23, 1, '1987-06-18 02:42:48', '2012-04-16 06:56:41', NULL),
(24, 24, 1, '1974-06-26 02:52:23', '2017-09-14 07:51:42', NULL),
(25, 25, 1, '2012-07-08 03:04:04', '1992-09-24 00:52:48', NULL),
(26, 26, 1, '1975-02-23 21:22:31', '1972-01-19 02:00:31', NULL),
(27, 27, 1, '2016-06-20 12:49:52', '2002-01-21 23:35:52', NULL),
(28, 28, 1, '1995-03-27 23:04:09', '1998-01-06 09:49:53', NULL),
(29, 29, 1, '2004-09-23 07:37:10', '2007-11-04 09:12:57', NULL),
(30, 30, 1, '1992-06-23 20:06:39', '1975-06-25 06:46:52', NULL),
(31, 31, 1, '1990-06-04 19:43:41', '1998-10-11 22:19:41', NULL),
(32, 32, 1, '2010-09-05 13:23:50', '2012-05-29 19:34:31', NULL),
(33, 33, 1, '1982-04-18 21:15:34', '1973-03-30 07:47:56', NULL),
(34, 34, 1, '1993-12-18 16:28:06', '1977-12-08 01:18:34', NULL),
(35, 35, 1, '1975-01-25 03:10:31', '2017-07-12 07:57:51', NULL),
(36, 36, 1, '2001-06-21 06:05:56', '2014-01-02 00:32:31', NULL),
(37, 37, 1, '1983-11-06 17:11:45', '2013-08-22 03:40:06', NULL),
(38, 38, 1, '1994-07-26 21:14:48', '1972-05-13 12:06:58', NULL),
(39, 39, 1, '2011-10-28 15:48:31', '1980-07-03 08:46:36', NULL),
(40, 40, 1, '1994-10-06 23:42:37', '1991-08-12 02:33:30', NULL),
(41, 41, 1, '1982-12-21 18:17:17', '2009-07-03 21:36:19', NULL),
(42, 42, 1, '1989-07-09 17:59:22', '1977-10-27 19:56:59', NULL),
(43, 43, 1, '1997-11-24 23:04:20', '1996-04-13 16:23:23', NULL),
(44, 44, 1, '1981-11-16 04:12:26', '2013-11-06 02:07:02', NULL),
(45, 45, 1, '1975-03-04 02:40:14', '1999-08-15 05:22:37', NULL),
(46, 46, 1, '2005-03-12 14:31:31', '1972-09-08 22:26:05', NULL),
(47, 47, 1, '2005-02-02 10:11:06', '2017-09-10 13:30:06', NULL),
(48, 48, 1, '2001-03-06 15:15:17', '1984-12-11 14:20:05', NULL),
(49, 49, 1, '2011-04-17 11:14:48', '1974-11-20 18:46:26', NULL),
(50, 50, 1, '2008-10-12 15:08:16', '1976-06-10 07:27:21', NULL),
(51, 51, 1, '1975-02-24 19:39:07', '2011-05-06 16:32:30', NULL),
(52, 52, 1, '1995-12-14 07:39:40', '2009-02-25 11:56:23', NULL),
(53, 53, 1, '2007-10-16 14:00:30', '1984-10-16 23:42:08', NULL),
(54, 54, 1, '1973-03-15 06:21:30', '1984-07-16 15:44:03', NULL),
(55, 55, 1, '1991-08-28 18:50:15', '1977-07-10 14:13:28', NULL),
(56, 56, 1, '1984-12-04 23:58:52', '1987-10-14 21:22:30', NULL),
(57, 57, 1, '2016-09-06 18:23:30', '2002-05-24 03:22:43', NULL),
(58, 58, 1, '2014-04-28 00:24:09', '1977-05-31 03:47:29', NULL),
(59, 59, 1, '1995-08-17 00:25:28', '1970-06-05 21:58:03', NULL),
(60, 60, 1, '2005-10-20 19:08:30', '2008-06-16 10:13:57', NULL),
(61, 61, 1, '2015-08-22 03:18:54', '1983-01-14 16:48:22', NULL),
(62, 62, 1, '1972-03-11 16:21:03', '1980-04-17 05:53:13', NULL),
(63, 1, 5, '1984-12-12 14:57:48', '1991-12-26 12:04:34', NULL),
(64, 2, 5, '1982-12-02 01:16:31', '1973-06-22 08:16:53', NULL),
(65, 57, 5, '1991-12-03 11:18:27', '1981-11-11 02:30:22', NULL),
(66, 63, 5, '1970-08-22 10:27:56', '1979-11-03 07:43:35', NULL),
(67, 64, 5, '1988-08-01 14:17:07', '1997-12-27 02:08:59', NULL),
(68, 65, 5, '2015-05-08 09:58:49', '1980-04-11 13:28:35', NULL),
(69, 60, 5, '1971-05-08 18:46:44', '2007-08-13 05:37:17', NULL),
(70, 61, 5, '2014-10-16 16:36:58', '2012-12-18 13:35:18', NULL),
(71, 51, 5, '1977-07-23 08:10:22', '1977-07-25 02:10:07', NULL),
(72, 53, 5, '1988-10-30 21:18:38', '1982-10-27 13:17:31', NULL),
(73, 66, 5, '1997-06-19 12:32:53', '2000-05-20 01:48:42', NULL),
(74, 58, 5, '2010-01-11 07:23:31', '2005-06-17 13:46:10', NULL),
(75, 62, 5, '1972-06-29 05:13:18', '1975-12-20 18:05:12', NULL),
(76, 1, 6, '2001-05-17 20:00:28', '1993-12-20 17:21:15', NULL),
(77, 2, 6, '1997-02-09 05:05:43', '1974-04-13 09:18:58', NULL),
(78, 55, 6, '1990-09-14 20:51:41', '2002-02-16 20:21:49', NULL),
(79, 67, 6, '2001-12-24 17:55:37', '2006-04-09 13:59:01', NULL),
(80, 56, 6, '1981-03-22 14:19:41', '2013-02-28 21:11:09', NULL),
(81, 68, 6, '2008-09-24 18:10:52', '1972-01-11 11:34:52', NULL),
(82, 58, 6, '1989-01-03 18:04:39', '1979-04-15 23:29:00', NULL),
(83, 69, 6, '1971-08-03 01:14:46', '2006-03-18 14:52:33', NULL),
(84, 70, 6, '1971-03-24 14:30:32', '1985-01-22 17:26:59', NULL),
(85, 1, 8, '2012-02-11 07:31:09', '1978-05-15 12:23:25', NULL),
(86, 2, 8, '1981-06-02 05:29:34', '1970-11-25 08:26:52', NULL),
(87, 25, 8, '1994-04-17 14:22:10', '1982-11-01 01:29:34', NULL),
(88, 71, 8, '2019-10-15 00:41:39', '2012-02-24 14:53:22', NULL),
(89, 26, 8, '2005-12-03 15:22:40', '1976-06-14 01:46:55', NULL),
(90, 72, 8, '1979-03-16 22:04:02', '2017-09-01 04:55:55', NULL),
(91, 73, 8, '1987-05-11 10:45:15', '1996-10-18 00:31:04', NULL),
(92, 60, 8, '2011-06-13 05:02:50', '1995-06-09 10:56:14', NULL),
(93, 61, 8, '2012-08-04 19:40:03', '1979-04-26 09:51:14', NULL),
(94, 74, 8, '2002-10-20 08:57:37', '1976-07-28 17:46:26', NULL),
(95, 75, 8, '1988-07-15 06:02:55', '2000-02-27 12:01:19', NULL),
(96, 58, 8, '1999-10-11 19:44:04', '2006-03-16 08:41:10', NULL),
(97, 76, 8, '2013-11-22 20:12:16', '1982-06-23 22:02:31', NULL),
(98, 77, 8, '1979-09-18 18:01:27', '1973-06-15 00:31:03', NULL),
(99, 78, 8, '2005-07-09 17:23:07', '1980-01-20 17:11:30', NULL),
(100, 69, 8, '1993-01-25 14:41:05', '2014-04-25 03:58:00', NULL),
(101, 79, 8, '2013-02-03 07:06:46', '2018-09-14 01:21:29', NULL),
(102, 80, 8, '2012-08-20 20:57:39', '1986-06-19 05:27:58', NULL),
(103, 32, 8, '1986-08-30 04:28:11', '2005-05-02 04:52:52', NULL),
(104, 81, 8, '2002-06-01 18:23:11', '1999-04-09 12:00:08', NULL),
(105, 1, 7, '2012-05-09 21:01:06', '1970-09-15 12:02:07', NULL),
(106, 2, 7, '1989-05-02 06:32:14', '1991-05-20 13:59:26', NULL),
(107, 28, 7, '2018-03-11 17:12:51', '2002-12-09 11:43:20', NULL),
(108, 82, 7, '2007-05-05 03:08:47', '2006-01-08 05:23:08', NULL),
(109, 29, 7, '1991-11-26 07:12:17', '1980-04-30 09:56:38', NULL),
(110, 80, 7, '1976-02-06 20:18:10', '1988-05-18 09:18:43', NULL),
(111, 83, 7, '2009-11-21 11:45:56', '2015-02-13 12:01:47', NULL),
(112, 32, 7, '2013-10-01 09:07:51', '2008-05-13 23:20:22', NULL),
(113, 81, 7, '2016-12-26 10:02:18', '1993-03-31 20:27:50', NULL),
(114, 60, 7, '2005-03-05 00:46:14', '2006-12-06 04:15:06', NULL),
(115, 78, 7, '1984-04-28 18:37:53', '1977-10-10 22:28:07', NULL),
(116, 1, 2, '2016-09-14 07:14:55', '2013-12-14 14:06:15', NULL),
(117, 2, 2, '2000-05-03 10:53:50', '2018-06-13 02:30:39', NULL),
(118, 84, 2, '1998-09-20 10:47:17', '2012-05-05 19:15:00', NULL),
(119, 5, 2, '1976-10-25 14:15:00', '1970-08-27 01:13:41', NULL),
(120, 6, 2, '1988-11-27 12:39:56', '1990-02-07 04:57:18', NULL),
(121, 7, 2, '1977-11-13 13:43:55', '1982-12-12 16:59:12', NULL),
(122, 8, 2, '2007-06-15 14:01:44', '2013-08-05 21:39:13', NULL),
(123, 85, 2, '2004-07-12 14:08:18', '1981-06-13 05:50:49', NULL),
(124, 9, 2, '1990-10-30 11:27:28', '1972-01-05 16:57:53', NULL),
(125, 10, 2, '2017-02-08 22:34:48', '2009-11-09 12:19:04', NULL),
(126, 11, 2, '1994-08-02 21:14:19', '2016-08-19 09:33:18', NULL),
(127, 12, 2, '1988-03-04 11:28:43', '2014-09-20 14:13:05', NULL),
(128, 86, 2, '1987-09-20 13:18:54', '1989-05-30 17:30:50', NULL),
(129, 13, 2, '1996-04-01 04:30:59', '1978-12-15 21:22:39', NULL),
(130, 14, 2, '2014-07-24 21:22:58', '1970-02-13 18:45:00', NULL),
(131, 15, 2, '2013-04-20 16:00:41', '1979-09-20 08:12:12', NULL),
(132, 16, 2, '1974-11-21 05:50:11', '1984-06-17 02:25:51', NULL),
(133, 87, 2, '2008-06-07 00:54:48', '1991-10-07 10:41:50', NULL),
(134, 17, 2, '2004-01-08 04:24:15', '1997-03-02 18:26:21', NULL),
(135, 18, 2, '2006-06-25 14:52:45', '1979-02-02 06:16:35', NULL),
(136, 19, 2, '1991-02-05 06:29:25', '1997-02-20 17:50:25', NULL),
(137, 20, 2, '1998-12-02 11:02:37', '1997-07-10 19:43:26', NULL),
(138, 88, 2, '2000-11-16 12:57:57', '2008-05-18 00:26:42', NULL),
(139, 21, 2, '1988-06-18 09:09:23', '1993-08-26 21:04:35', NULL),
(140, 22, 2, '1974-03-08 16:29:22', '1978-06-15 16:02:05', NULL),
(141, 23, 2, '2010-09-03 01:58:29', '2005-11-02 05:13:05', NULL),
(142, 24, 2, '2012-03-17 11:05:48', '1979-10-09 16:16:09', NULL),
(143, 89, 2, '2002-12-22 01:00:21', '2018-05-11 13:44:20', NULL),
(144, 25, 2, '1988-10-13 16:39:33', '1975-11-16 20:07:27', NULL),
(145, 26, 2, '1971-05-16 21:06:59', '1994-07-31 14:11:55', NULL),
(146, 27, 2, '2011-11-19 15:31:09', '1983-10-30 04:11:04', NULL),
(147, 90, 2, '1991-02-27 21:20:46', '1981-12-12 02:47:29', NULL),
(148, 28, 2, '1992-02-20 16:35:17', '1983-08-23 02:55:32', NULL),
(149, 29, 2, '2000-05-01 13:36:18', '1985-09-20 15:50:17', NULL),
(150, 30, 2, '2017-08-20 22:13:56', '1987-10-14 15:20:19', NULL),
(151, 31, 2, '1981-05-16 02:52:25', '2008-05-27 13:34:58', NULL),
(152, 51, 2, '2003-12-14 09:22:56', '2005-03-01 12:20:08', NULL),
(153, 52, 2, '1978-01-29 09:59:02', '1998-05-30 11:50:24', NULL),
(154, 53, 2, '1979-10-09 14:12:03', '2006-01-27 05:36:08', NULL),
(155, 55, 2, '2003-11-20 01:28:34', '2009-09-26 02:36:22', NULL),
(156, 56, 2, '2000-06-11 17:55:52', '1986-02-24 03:02:56', NULL),
(157, 32, 2, '2006-08-23 13:22:36', '1982-09-10 17:31:04', NULL),
(158, 57, 2, '1988-04-06 01:40:23', '2007-08-13 02:38:06', NULL),
(159, 60, 2, '2010-05-15 18:02:10', '1990-02-15 17:10:46', NULL),
(160, 61, 2, '2017-12-17 22:17:04', '1970-07-26 04:49:42', NULL),
(161, 58, 2, '1982-09-24 16:45:56', '2018-05-23 20:03:08', NULL),
(162, 62, 2, '1971-11-24 21:33:25', '1996-03-21 11:09:15', NULL),
(163, 1, 3, '1982-04-08 23:34:25', '2011-09-13 14:06:53', NULL),
(164, 2, 3, '1975-07-15 18:57:46', '1975-06-09 04:40:34', NULL),
(165, 85, 3, '2014-12-19 03:04:05', '1989-09-26 23:01:05', NULL),
(166, 9, 3, '1990-02-15 17:21:59', '2000-02-26 13:48:10', NULL),
(167, 10, 3, '1984-11-24 23:27:43', '2010-09-16 08:40:05', NULL),
(168, 11, 3, '2019-01-09 12:10:20', '2018-02-06 00:15:25', NULL),
(169, 12, 3, '2002-08-05 06:42:07', '1989-11-21 13:47:38', NULL),
(170, 86, 3, '1984-09-18 09:55:48', '2019-06-08 18:44:34', NULL),
(171, 13, 3, '2006-01-14 01:20:50', '1983-03-02 19:08:24', NULL),
(172, 14, 3, '1973-05-29 08:31:38', '2015-07-04 09:03:49', NULL),
(173, 15, 3, '1975-07-11 03:21:38', '1994-09-26 21:19:59', NULL),
(174, 16, 3, '2016-04-27 05:16:04', '2008-05-14 02:17:15', NULL),
(175, 87, 3, '2011-05-06 17:12:07', '2011-10-30 08:17:05', NULL),
(176, 17, 3, '2010-03-07 05:24:10', '2009-10-30 08:06:07', NULL),
(177, 18, 3, '2006-02-20 20:02:52', '2001-08-26 17:10:43', NULL),
(178, 19, 3, '1992-08-22 23:16:17', '2004-09-01 16:41:22', NULL),
(179, 20, 3, '1975-05-13 19:48:58', '2005-08-02 00:06:34', NULL),
(180, 88, 3, '2008-02-10 20:14:11', '2007-10-08 05:05:48', NULL),
(181, 21, 3, '1995-11-29 21:04:12', '2013-05-09 08:49:41', NULL),
(182, 22, 3, '1995-05-31 11:39:41', '1996-05-05 12:18:48', NULL),
(183, 23, 3, '1975-01-02 05:26:36', '1970-03-10 18:34:56', NULL),
(184, 24, 3, '2018-12-30 04:17:21', '1979-10-19 15:10:53', NULL),
(185, 89, 3, '1973-02-04 18:21:35', '1974-08-21 22:26:46', NULL),
(186, 25, 3, '1970-02-11 04:46:43', '1984-04-13 10:13:02', NULL),
(187, 26, 3, '2006-12-13 02:06:40', '2018-10-10 14:41:45', NULL),
(188, 27, 3, '2002-11-20 05:25:19', '1982-07-14 03:28:00', NULL),
(189, 90, 3, '2017-04-23 17:46:55', '2012-10-27 20:41:34', NULL),
(190, 28, 3, '1992-12-03 13:09:29', '1986-02-10 16:41:01', NULL),
(191, 29, 3, '1987-07-10 04:18:37', '1985-04-23 16:03:42', NULL),
(192, 30, 3, '1988-12-18 22:59:20', '2014-10-19 15:00:24', NULL),
(193, 51, 3, '1980-06-02 11:06:09', '2009-12-15 05:02:08', NULL),
(194, 52, 3, '1987-10-05 09:13:12', '1998-09-13 11:06:25', NULL),
(195, 53, 3, '2013-06-22 16:56:42', '1993-03-26 05:00:42', NULL),
(196, 91, 3, '1993-01-25 04:32:41', '1977-02-27 09:56:47', NULL),
(197, 55, 3, '1998-11-30 16:23:27', '2005-07-08 11:41:52', NULL),
(198, 56, 3, '1991-01-11 04:27:58', '1983-10-09 23:46:55', NULL),
(199, 32, 3, '1990-06-14 11:52:48', '2004-05-01 13:37:41', NULL),
(200, 57, 3, '1975-05-21 16:25:37', '1983-10-18 23:00:19', NULL),
(201, 60, 3, '1994-07-29 03:56:59', '2010-11-25 16:51:23', NULL),
(202, 61, 3, '1999-08-10 16:37:34', '2019-10-08 03:38:34', NULL),
(203, 58, 3, '2006-03-06 14:40:17', '1985-11-10 21:42:31', NULL),
(204, 62, 3, '1970-07-24 06:21:21', '2016-06-12 14:40:40', NULL),
(205, 92, 3, '2014-06-09 22:25:29', '1984-06-11 12:56:25', NULL),
(206, 1, 4, '2017-04-24 10:48:25', '1974-12-13 06:31:12', NULL),
(207, 2, 4, '2015-03-27 01:28:32', '1980-10-13 09:14:37', NULL),
(208, 86, 4, '1970-08-10 05:19:00', '1973-11-18 19:01:07', NULL),
(209, 13, 4, '1988-02-13 00:48:07', '1980-05-27 12:28:53', NULL),
(210, 15, 4, '1987-05-18 20:08:27', '1971-10-26 01:53:48', NULL),
(211, 87, 4, '1986-03-09 10:58:15', '2013-03-31 12:39:44', NULL),
(212, 17, 4, '1998-05-06 09:03:53', '2013-09-26 13:46:01', NULL),
(213, 19, 4, '2001-09-20 13:54:30', '2008-07-05 20:54:55', NULL),
(214, 88, 4, '2018-01-16 13:51:24', '1976-12-09 09:07:38', NULL),
(215, 21, 4, '2018-09-07 01:07:35', '2009-08-06 05:52:53', NULL),
(216, 23, 4, '2007-06-01 03:12:28', '1973-07-04 05:47:33', NULL),
(217, 89, 4, '1996-08-20 23:40:49', '1983-10-17 08:58:45', NULL),
(218, 25, 4, '1999-09-02 20:46:46', '1994-04-13 17:26:46', NULL),
(219, 26, 4, '1982-12-15 08:37:56', '1986-05-28 23:20:51', NULL),
(220, 90, 4, '1970-02-14 21:40:21', '2002-03-30 13:51:26', NULL),
(221, 28, 4, '2018-03-18 16:51:03', '2000-10-22 05:48:56', NULL),
(222, 29, 4, '2004-10-13 08:52:18', '2020-01-08 21:09:18', NULL),
(223, 31, 4, '2000-09-07 05:40:16', '1993-10-25 10:07:03', NULL),
(224, 93, 4, '1972-10-02 16:23:57', '1971-03-18 05:23:51', NULL),
(225, 51, 4, '2011-10-01 21:54:57', '1978-07-26 08:36:53', NULL),
(226, 53, 4, '2002-12-11 19:47:11', '2009-08-18 02:48:54', NULL),
(227, 72, 4, '2010-05-02 01:39:59', '1999-04-04 04:13:50', NULL),
(228, 73, 4, '1978-11-07 15:08:17', '2017-08-28 18:26:57', NULL),
(229, 94, 4, '2015-11-13 19:07:32', '2001-06-23 07:32:54', NULL),
(230, 55, 4, '2019-09-07 19:56:48', '1993-02-11 05:54:04', NULL),
(231, 56, 4, '2015-09-22 10:36:29', '2002-03-02 04:27:25', NULL),
(232, 74, 4, '2008-05-11 03:18:06', '2015-03-08 11:36:58', NULL),
(233, 75, 4, '2011-10-26 21:59:07', '2002-10-15 19:33:31', NULL),
(234, 92, 4, '2006-03-20 02:46:45', '1979-12-20 11:43:53', NULL),
(235, 95, 4, '1991-11-19 21:43:48', '1994-11-20 20:36:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_tutorsessions`
--

CREATE TABLE `request_tutorsessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `teacher_id` bigint(20) UNSIGNED DEFAULT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `occurred_date` varchar(20) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:pending, 1: approved, 2:rejected',
  `start_time` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request_tutorsession_student`
--

CREATE TABLE `request_tutorsession_student` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_tutorsession_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'systemadministrators', 'System Administrator', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(2, 'supportadministrators', 'Support Administrator', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(3, 'principals', 'Principals', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(4, 'inspectors', 'Inspectors', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(5, 'teachers', 'Teachers', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(6, 'tutors', 'Tutors', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(7, 'parents', 'Parents', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(8, 'students', 'Students', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `school_configurations`
--

CREATE TABLE `school_configurations` (
  `id` int(10) UNSIGNED NOT NULL,
  `institute_name` varchar(255) DEFAULT NULL,
  `established_date` varchar(20) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `copyright_text` varchar(255) DEFAULT NULL,
  `title_tag` varchar(255) DEFAULT NULL,
  `description_tag` text,
  `meta_tag` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `meta_keyword` text,
  `logo` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_configurations`
--

INSERT INTO `school_configurations` (`id`, `institute_name`, `established_date`, `owner`, `copyright_text`, `title_tag`, `description_tag`, `meta_tag`, `meta_description`, `meta_keyword`, `logo`, `icon`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ilox Academy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-02 05:05:24', '2019-10-14 00:43:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_grade`
--

CREATE TABLE `student_grade` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) UNSIGNED NOT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `is_activate` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active, 0: inactive	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptionfee_student`
--

CREATE TABLE `subscriptionfee_student` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `subscription_fee_id` int(10) UNSIGNED DEFAULT NULL,
  `paid_by` bigint(20) UNSIGNED DEFAULT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `amount` varchar(10) DEFAULT NULL,
  `currency` varchar(5) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `detail` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_fees`
--

CREATE TABLE `subscription_fees` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `currency` varchar(5) NOT NULL DEFAULT 'zar',
  `frequency` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: One Time, 2: Recurrent',
  `is_activate` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active, 0: inactive	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tutorsessions`
--

CREATE TABLE `tutorsessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `start_time` varchar(20) DEFAULT NULL,
  `end_time` varchar(20) DEFAULT NULL,
  `tutor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `occurred_date` varchar(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `chat_or_call_external_link` varchar(255) DEFAULT NULL,
  `external_verdor_name` varchar(255) DEFAULT NULL,
  `chat_or_call_saved_link` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: setup-need-to-complete,1: setup-complete, 2: class-passed/completed, 3: class-cancelled, 4: started',
  `request_tutorsession_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tutorsession_students`
--

CREATE TABLE `tutorsession_students` (
  `id` int(10) UNSIGNED NOT NULL,
  `tutorsession_id` int(10) UNSIGNED DEFAULT NULL,
  `tutor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_attended` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: attended, 0: not-attended',
  `rating` tinyint(1) DEFAULT '0' COMMENT '1 to 5',
  `comment` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `report_date` date DEFAULT NULL,
  `report_description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `zipcode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_joining` date DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_activate` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active, 0: inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `middle_name`, `email`, `contact_no`, `address`, `zipcode`, `date_of_joining`, `date_of_birth`, `parent_id`, `token`, `email_verified_at`, `password`, `api_token`, `remember_token`, `is_activate`, `created_at`, `updated_at`, `deleted_at`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`) VALUES
(1, 'System ', 'Admin', NULL, 'admin@admin.com', '9625874130', NULL, '98740', '2020-01-08', NULL, NULL, NULL, NULL, '$2y$10$2.Dvo8q0Lgkgl.jmXsYJleUFnndO0zQfgHTRiG6x14eR9oUF80kfe', NULL, NULL, 1, NULL, '2020-01-26 04:21:46', NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_years`
--
ALTER TABLE `academic_years`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_assignments_1_idx` (`classsession_id`),
  ADD KEY `fk_assignments_2_idx` (`session_id`),
  ADD KEY `fk_assignments_3_idx` (`grade_id`),
  ADD KEY `fk_assignments_4_idx` (`subject_id`),
  ADD KEY `fk_assignments_5_idx` (`teacher_id`);

--
-- Indexes for table `assignment_attachment`
--
ALTER TABLE `assignment_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_assignment_attachment_1_idx` (`assignment_id`);

--
-- Indexes for table `assignment_student`
--
ALTER TABLE `assignment_student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_assignment_student_1_idx` (`assignment_id`),
  ADD KEY `fk_assignment_student_2_idx` (`student_id`);

--
-- Indexes for table `assignment_student_attachment`
--
ALTER TABLE `assignment_student_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_assignment_student_attachment_1_idx` (`assignment_student_id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classsessions`
--
ALTER TABLE `classsessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_virtual_classes_1_idx` (`session_id`),
  ADD KEY `fk_virtual_classes_2_idx` (`grade_id`),
  ADD KEY `fk_virtual_classes_3_idx` (`subject_id`),
  ADD KEY `fk_virtual_classes_5_idx` (`teacher_id`);

--
-- Indexes for table `classsession_students`
--
ALTER TABLE `classsession_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_class_feedbacks_1_idx` (`classsession_id`),
  ADD KEY `fk_class_feedbacks_2_idx` (`teacher_id`),
  ADD KEY `fk_class_feedbacks_3_idx` (`student_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `fk_grades_1_idx` (`session_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_permission_role_1_idx` (`permission_id`),
  ADD KEY `fk_permission_role_2_idx` (`role_id`);

--
-- Indexes for table `request_tutorsessions`
--
ALTER TABLE `request_tutorsessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_request_virtual_classes_1_idx` (`grade_id`),
  ADD KEY `fk_request_virtual_classes_2_idx` (`session_id`),
  ADD KEY `fk_request_virtual_classes_3_idx` (`subject_id`),
  ADD KEY `fk_request_virtual_classes_4_idx` (`student_id`),
  ADD KEY `fk_request_virtual_classes_5_idx` (`teacher_id`);

--
-- Indexes for table `request_tutorsession_student`
--
ALTER TABLE `request_tutorsession_student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_request_virtual_class_student_1_idx` (`request_tutorsession_id`),
  ADD KEY `fk_request_virtual_class_student_2_idx` (`student_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_role_user_1_idx` (`user_id`),
  ADD KEY `fk_role_user_2_idx` (`role_id`);

--
-- Indexes for table `school_configurations`
--
ALTER TABLE `school_configurations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_grade`
--
ALTER TABLE `student_grade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_grades_1_idx` (`student_id`),
  ADD KEY `fk_student_grades_2_idx` (`session_id`),
  ADD KEY `fk_student_grades_3_idx` (`grade_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `fk_subjects_1_idx` (`grade_id`);

--
-- Indexes for table `subscriptionfee_student`
--
ALTER TABLE `subscriptionfee_student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subscriptionfee_student_1_idx` (`session_id`),
  ADD KEY `fk_subscriptionfee_student_2_idx` (`grade_id`),
  ADD KEY `fk_subscriptionfee_student_3_idx` (`subscription_fee_id`),
  ADD KEY `fk_subscriptionfee_student_4_idx` (`paid_by`),
  ADD KEY `fk_subscriptionfee_student_6_idx` (`student_id`);

--
-- Indexes for table `subscription_fees`
--
ALTER TABLE `subscription_fees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subscription_fees_1_idx` (`session_id`),
  ADD KEY `fk_subscription_fees_2_idx` (`grade_id`);

--
-- Indexes for table `tutorsessions`
--
ALTER TABLE `tutorsessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_virtual_classes_1_idx` (`session_id`),
  ADD KEY `fk_virtual_classes_2_idx` (`grade_id`),
  ADD KEY `fk_virtual_classes_3_idx` (`subject_id`),
  ADD KEY `fk_virtual_classes_5_idx` (`tutor_id`),
  ADD KEY `fk_tutorsessions_5_idx` (`request_tutorsession_id`);

--
-- Indexes for table `tutorsession_students`
--
ALTER TABLE `tutorsession_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_class_feedbacks_1_idx` (`tutorsession_id`),
  ADD KEY `fk_class_feedbacks_2_idx` (`tutor_id`),
  ADD KEY `fk_class_feedbacks_3_idx` (`student_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_years`
--
ALTER TABLE `academic_years`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assignment_attachment`
--
ALTER TABLE `assignment_attachment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assignment_student`
--
ALTER TABLE `assignment_student`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assignment_student_attachment`
--
ALTER TABLE `assignment_student_attachment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classsessions`
--
ALTER TABLE `classsessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classsession_students`
--
ALTER TABLE `classsession_students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT for table `request_tutorsessions`
--
ALTER TABLE `request_tutorsessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_tutorsession_student`
--
ALTER TABLE `request_tutorsession_student`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `school_configurations`
--
ALTER TABLE `school_configurations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student_grade`
--
ALTER TABLE `student_grade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscriptionfee_student`
--
ALTER TABLE `subscriptionfee_student`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscription_fees`
--
ALTER TABLE `subscription_fees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tutorsessions`
--
ALTER TABLE `tutorsessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tutorsession_students`
--
ALTER TABLE `tutorsession_students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `fk_assignments_1` FOREIGN KEY (`classsession_id`) REFERENCES `classsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignments_2` FOREIGN KEY (`session_id`) REFERENCES `academic_years` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignments_3` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignments_4` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignments_5` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assignment_attachment`
--
ALTER TABLE `assignment_attachment`
  ADD CONSTRAINT `fk_assignment_attachment_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assignment_student`
--
ALTER TABLE `assignment_student`
  ADD CONSTRAINT `fk_assignment_student_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignment_student_2` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assignment_student_attachment`
--
ALTER TABLE `assignment_student_attachment`
  ADD CONSTRAINT `fk_assignment_student_attachment_1` FOREIGN KEY (`assignment_student_id`) REFERENCES `assignment_student` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `classsessions`
--
ALTER TABLE `classsessions`
  ADD CONSTRAINT `fk_virtual_classes_1` FOREIGN KEY (`session_id`) REFERENCES `academic_years` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_virtual_classes_2` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_virtual_classes_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_virtual_classes_5` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `classsession_students`
--
ALTER TABLE `classsession_students`
  ADD CONSTRAINT `fk_class_feedbacks_1` FOREIGN KEY (`classsession_id`) REFERENCES `classsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_class_feedbacks_2` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_class_feedbacks_3` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `fk_grades_1` FOREIGN KEY (`session_id`) REFERENCES `academic_years` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `fk_permission_role_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_permission_role_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `request_tutorsessions`
--
ALTER TABLE `request_tutorsessions`
  ADD CONSTRAINT `fk_request_virtual_classes_1` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_request_virtual_classes_2` FOREIGN KEY (`session_id`) REFERENCES `academic_years` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_request_virtual_classes_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_request_virtual_classes_4` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_request_virtual_classes_5` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `request_tutorsession_student`
--
ALTER TABLE `request_tutorsession_student`
  ADD CONSTRAINT `fk_request_virtual_class_student_1` FOREIGN KEY (`request_tutorsession_id`) REFERENCES `request_tutorsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_request_virtual_class_student_2` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `fk_role_user_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_role_user_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `student_grade`
--
ALTER TABLE `student_grade`
  ADD CONSTRAINT `fk_student_grades_1` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_student_grades_2` FOREIGN KEY (`session_id`) REFERENCES `academic_years` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_student_grades_3` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `fk_subjects_1` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subscriptionfee_student`
--
ALTER TABLE `subscriptionfee_student`
  ADD CONSTRAINT `fk_subscriptionfee_student_1` FOREIGN KEY (`session_id`) REFERENCES `academic_years` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscriptionfee_student_2` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscriptionfee_student_3` FOREIGN KEY (`subscription_fee_id`) REFERENCES `subscription_fees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscriptionfee_student_4` FOREIGN KEY (`paid_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscriptionfee_student_6` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subscription_fees`
--
ALTER TABLE `subscription_fees`
  ADD CONSTRAINT `fk_subscription_fees_1` FOREIGN KEY (`session_id`) REFERENCES `academic_years` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscription_fees_2` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tutorsessions`
--
ALTER TABLE `tutorsessions`
  ADD CONSTRAINT `fk_tutorsessions_1` FOREIGN KEY (`session_id`) REFERENCES `academic_years` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tutorsessions_2` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tutorsessions_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tutorsessions_4` FOREIGN KEY (`tutor_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tutorsessions_5` FOREIGN KEY (`request_tutorsession_id`) REFERENCES `request_tutorsessions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tutorsession_students`
--
ALTER TABLE `tutorsession_students`
  ADD CONSTRAINT `fk_tutorsession_students_1` FOREIGN KEY (`tutorsession_id`) REFERENCES `tutorsessions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tutorsession_students_2` FOREIGN KEY (`tutor_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tutorsession_students_3` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;